import cv2
import os
import torch
from PIL import Image
import lib.transforms as T
from torchvision.transforms import functional as F
from IPython import embed
import time
import numpy as np
from lib.visualize import visualize_detections
from lib.data.classes import Classes


def show(image):
    cv2.imshow('', image)
    cv2.waitKey()
    cv2.destroyAllWindows()


device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

# model = torch.load('models/2021-3-22_1-23-13/19_full_model.pt')
model = torch.load('models/2021-4-8_5-43-21/7_full_model.pt')
model.to(device)
model.eval()

data_path = '/media/david/A/Datasets/PvP/Test videos/dataset/robery_test'

score_thr = 0.7

for i, im_name in enumerate(os.listdir(data_path)):

    # if i > 50:
    #     break

    image = Image.open(os.path.join(data_path, im_name)).convert("RGB")

    image_scale = 720

    w, h = image.size
    r = 1
    if w != image_scale or h != image_scale:
        if w > h:
            new_w = image_scale
            r = new_w / w
            new_h = h * r
            image = image.resize((round(new_w), round(new_h)))
        else:
            new_h = image_scale
            r = new_h / h
            new_w = w * r
            image = image.resize((round(new_w), round(new_h)))

    # image.show()

    image_tensor = F.to_tensor(image)
    image_tensor = image_tensor.unsqueeze(0)

    image_tensor = image_tensor.to(device)

    start = time.time()

    predictions = model(image_tensor)

    end = time.time()
    print ('Time: {}\tImage size:'.format(end - start, image_tensor.shape))

    predictions = predictions[0]

    bboxes = predictions['boxes'].cpu().detach().numpy()
    labels = predictions['labels'].cpu().numpy()
    scores = predictions['scores'].cpu().detach().numpy()

    bboxes = bboxes[scores > score_thr]
    labels = labels[scores > score_thr]
    scores = scores[scores > score_thr]

    class_names = np.array([Classes.reverse_joined_classes[x] for x in labels])

    visualize_detections(
        image,
        bboxes,
        class_names,
        scores,
        save=''
    )