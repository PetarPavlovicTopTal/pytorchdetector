
def write_to_xml(
        save_path,
        folder,
        filename,
        path,
        width,
        height,
        depth,
        objects,
):
        template = '''<annotation>
	<folder>{}</folder>
	<filename>{}</filename>
	<path>{}</path>
	<source>
		<database>Unknown</database>
	</source>
	<size>
		<width>{}</width>
		<height>{}</height>
		<depth>{}</depth>
	</size>
	<segmented>0</segmented>'''
        object_template = '''	
    <object>
		<name>{}</name>
		<pose>Unspecified</pose>
		<truncated>0</truncated>
		<difficult>0</difficult>
		<bndbox>
			<xmin>{}</xmin>
			<ymin>{}</ymin>
			<xmax>{}</xmax>
			<ymax>{}</ymax>
		</bndbox>
	</object>'''

        template_filled = template.format(
            folder,
            filename,
            path,
            width,
            height,
            depth,
        )

        for obj in objects:
            template_filled += object_template.format(
                obj[0], obj[1], obj[2], obj[3], obj[4]
            )

        template_filled += '''
</annotation>'''

        with open(save_path, 'w') as f:
            f.writelines(template_filled)
