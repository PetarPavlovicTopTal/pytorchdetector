import numpy as np
from IPython import embed


class CentroidTracker:
    def __init__(
            self,
            distance_threshold = 50,
            box_dependent_distance = True,
            box_distance_multiplier = 3,
            keep_tracked_detection = 5,
    ):
        self._tracked_detections = dict()
        self._active_ids = []
        self._free_ids = [x for x in range(1, 101)]
        self._distance_threshold = distance_threshold
        self._box_dependent_distance = True
        self._box_distance_multiplier = box_distance_multiplier
        self._keep_tracked_detection = keep_tracked_detection

    def get_centroids(self, bboxes):
        return np.hstack(((bboxes[:, 2] + bboxes[:, 0]).reshape(-1, 1) / 2,
                          (bboxes[:, 3] + bboxes[:, 1]).reshape(-1, 1) / 2))

    def get_centroid(self, bbox):
        return np.array([(bbox[2] + bbox[0]) / 2, (bbox[3] + bbox[1] / 2)])

    def get_next_id(self):
        new_id = self._free_ids[0]
        self._active_ids.append(new_id)
        self._free_ids = self._free_ids[1:]
        return new_id

    def deactivate_id(self, id):
        self._active_ids.remove(id)
        self._free_ids = [id] + self._free_ids

    def distance(self, c1, c2):
        return np.sqrt((c1[0] - c2[0]) * (c1[0] - c2[0]) + (c1[1] - c2[1]) * (c1[1] - c2[1]))

    def multiple_distances(self, c, multiple_cs):
        return np.sqrt((c[0] - multiple_cs[:, 0]) * (c[0] - multiple_cs[:, 0]) +
                       (c[1] - multiple_cs[:, 1]) * (c[1] - multiple_cs[:, 1]))

    def output_tracked_detections(self):
        detections = np.array([]).reshape(-1, 5)
        for key, value in self._tracked_detections.items():
            detections = np.append(detections, np.hstack((value['bbox'], np.array(key)))).reshape(-1, 5)
        return detections

    def get_new_detection(self, bbox):
        new_tracking_object = {
            'bbox': bbox,
            'time': 1,
            'lost': False,
            'lost_time': 0,
            'taken': True,
            'centroid': self.get_centroid(bbox),
        }
        if self._box_dependent_distance:
            w = bbox[2] - bbox[0]
            h = bbox[3] - bbox[1]
            new_tracking_object['threshold'] = (w + h) / 2 * self._box_distance_multiplier
        self._tracked_detections[self.get_next_id()] = new_tracking_object

    def match_last_frame_detections(self, detections):
        if len(detections) == 0 and len(self._tracked_detections) == 0:
            return detections
        if len(self._tracked_detections) == 0:
            for detection in detections:
                self.get_new_detection(detection)
            return self.output_tracked_detections()

        for value in self._tracked_detections.values():
            value['taken'] = False

        detected_centroids = self.get_centroids(detections)

        for detection, center in zip(detections, detected_centroids):
            min_distance = 100000
            min_distance_key = None
            for key, value in self._tracked_detections.items():
                if value['taken']:
                    continue
                distance = self.distance(center, value['centroid'])
                if distance < min_distance:
                    min_distance = distance
                    min_distance_key = key
            if self._box_dependent_distance:
                threshold = value['threshold']
            else:
                threshold = self._distance_threshold
            if min_distance < threshold:
                self._tracked_detections[min_distance_key]['taken'] = True
                self._tracked_detections[min_distance_key]['bbox'] = detection
                self._tracked_detections[min_distance_key]['centroid'] = center
                self._tracked_detections[min_distance_key]['time'] += 1
                self._tracked_detections[min_distance_key]['lost'] = False
                self._tracked_detections[min_distance_key]['lost_time'] = 0
                if self._box_dependent_distance:
                    w = self._tracked_detections[min_distance_key]['bbox'][2] - self._tracked_detections[min_distance_key]['bbox'][0]
                    h = self._tracked_detections[min_distance_key]['bbox'][3] - self._tracked_detections[min_distance_key]['bbox'][1]
                    self._tracked_detections[min_distance_key]['threshold'] = (w + h) / 2 * self._box_distance_multiplier
            else:
                self.get_new_detection(detection)

        delete_keys = []
        for key, value in self._tracked_detections.items():
            if not value['taken']:
                value['time'] += 1
                value['lost'] = True
                value['lost_time'] += 1
                if value['lost_time'] >= self._keep_tracked_detection:
                    self.deactivate_id(key)
                    delete_keys.append(key)

        for key in delete_keys:
            del self._tracked_detections[key]

        return self.output_tracked_detections()
