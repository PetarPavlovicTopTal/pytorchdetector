import numpy as np
import os
from PIL import Image
import torch
from xml.dom.minidom import parse
from lib.data.classes import Classes


class MarkDataset(torch.utils.data.Dataset):
    def __init__(
            self,
            root,
            images,
            annotations,
            transforms=None):
        self.root        = root
        self.images      = images
        self.annotations = annotations
        self.transforms  = transforms

        self.imgs     = list(sorted(os.listdir(os.path.join(self.root, self.images))))
        self.bbox_xml = list(sorted(os.listdir(os.path.join(self.root, self.annotations))))

    def __getitem__(self, idx):
        img_path = os.path.join(self.root, self.images, self.imgs[idx])
        bbox_xml_path = os.path.join(self.root, self.annotations, self.bbox_xml[idx])
        img = Image.open(img_path).convert("RGB")

        w, h = img.size
        r = 1
        if w > 720 or h > 720:
            if w > h:
                new_w = 720
                r = new_w / w
                new_h = h * r
                img = img.resize((round(new_w), round(new_h)))
            else:
                new_h = 720
                r = new_h / h
                new_w = w * r
                img = img.resize((round(new_w), round(new_h)))

        dom = parse(bbox_xml_path)
        data = dom.documentElement
        objects = data.getElementsByTagName('object')

        boxes = []
        labels = []
        for object_ in objects:
            name = object_.getElementsByTagName('name')[0].childNodes[0].nodeValue  # Is label, mark_type_1 or mark_type_2

            cls = Classes.class_join[Classes.reverse_classes[name]]
            if cls == None:
                continue

            labels.append(cls)

            bndbox = object_.getElementsByTagName('bndbox')[0]
            xmin = np.float(bndbox.getElementsByTagName('xmin')[0].childNodes[0].nodeValue) * r
            ymin = np.float(bndbox.getElementsByTagName('ymin')[0].childNodes[0].nodeValue) * r
            xmax = np.float(bndbox.getElementsByTagName('xmax')[0].childNodes[0].nodeValue) * r
            ymax = np.float(bndbox.getElementsByTagName('ymax')[0].childNodes[0].nodeValue) * r
            boxes.append([xmin, ymin, xmax, ymax])

        if len(labels) == 0:
            labels = [0]
            boxes = [[10.0, 10.0, 20.0, 20.0]]

        boxes = torch.as_tensor(boxes, dtype=torch.float32)
        labels = torch.as_tensor(labels, dtype=torch.int64)

        image_id = torch.tensor([idx])
        area = (boxes[:, 3] - boxes[:, 1]) * (boxes[:, 2] - boxes[:, 0])
        iscrowd = torch.zeros((max(1, len(objects)),), dtype=torch.int64)

        target = {}
        target["boxes"] = boxes
        target["labels"] = labels
        # Since you are training a target detection network, there is no target [masks] = masks in the tutorial
        target["image_id"] = image_id
        target["area"] = area
        target["iscrowd"] = iscrowd

        if self.transforms is not None:
            # Note that target (including bbox) is also transformed\enhanced here, which is different from transforms from torchvision import
            # Https://github.com/pytorch/vision/tree/master/references/detectionOfTransforms.pyThere are examples of target transformations when RandomHorizontalFlip
            img, target = self.transforms(img, target)

        return img, target

    def __len__(self):
        return len(self.imgs)