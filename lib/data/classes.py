'''
In PyTorch 0 means backgroun class, where in Tensorflow it is just a regular class.

Classes class is meant to be used partly, as needed. Somewhere it makes sence to use only
reverse_classes, on other places only reverse_joined_classes and so on.

class_map -> has knowledge about all the different variations of a class
          -> hard and not hard classes are merged

reverse_classes -> is reverse of classs_map
                -> this is used to map/group all of those variations to a single class

class_join -> gives class value to grouped classes

reverse_joined_classes -> interpretes output class value to meaningfull string
'''

class Classes:
    class_map = {
        'AK47': ['AK47', 'AK 47', 'ak47', 'AK47HARD', 'AK 47_hard', 'AK47hard', 'ak47_hard', 'AK47_hard', 'AK47_Hard'],
        'AR15': ['AR 15', 'AR15', 'ar15', 'AR 15_hard', 'AR15HARD', 'AR15_Hard', 'AR15_hard', 'AR15hard', 'ar15_hard', 'hard AR15'],
        'Hunting': ['Hunting', 'hunting', 'Huntinghard', 'hunting_hard', 'hunting rifle', 'hunting rifle_hard', 'Hunting rifle_hard', 'Hunting rifle'],
        'Knife': ['Knife', 'Knives', 'knife', 'knive', 'knife_hard', 'Knifehard', 'sword'],
        'Pistol': ['Pistol', 'Pistols', 'pistol', 'gun', 'handgun', 'pistolhard', 'Pistolhard', 'PistolHARD', 'Pistol_Hard', 'PistolsHard', 'Pistols_hard', 'hard pistol', 'pistolHARD', 'pistol_hard'],
        'Revolver': ['Revolver', 'revolver', 'revolver_', 'Revolverhard', 'RevolverHARD', 'Revolver_Hard', 'revolver_hard'],
        'Rifle': ['Rifle', 'Rifles', 'rifle', 'Riflehard', 'RifleHARD', 'hard rifle', 'rifle_hard'],
        'Shotgun': ['Shotgun', 'shotgun', 'shutgun', 'ShotgunHARD', 'Shotgun_Hard', 'Shotgun_hard', 'Shotgunhard', 'hard shotgun', 'shotgun_hard'],
        'Unknown': ['Unknown', 'unknown', 'unkonwn'],

        'person' : ['person'],
        'car'    : ['car'],
        'truck'  : ['truck'],
        'bus'    : ['bus'],
        'bicycle': ['bicycle'],
        'motorcycle': ['motorcycle'],
    }

    reverse_classes = {value: key for key, value_list in class_map.items() for value in value_list}

    class_join = {
        'AK47'       : 0,
        'AR15'       : 0,
        'Hunting'    : 0,
        'Knife'      : 0,
        'Pistol'     : 1,
        'Revolver'   : 1,
        'Rifle'      : 0,
        'Shotgun'    : 0,
        'Unknown'    : 0,
        'person'     : 0,
        'car'        : 0,
        'truck'      : 0,
        'bus'        : 0,
        'bicycle'    : 0,
        'motorcycle' : 0,
    }

    reverse_joined_classes = {
        1: 'Pistol',
    }