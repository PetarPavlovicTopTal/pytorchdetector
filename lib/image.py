import cv2


def resize_np(image, image_scale):
    h, w, _ = image.shape
    r = 1
    if w != image_scale or h != image_scale:
        if w > h:
            new_w = image_scale
            r = new_w / w
            new_h = h * r
            image = cv2.resize(image, (round(new_w), round(new_h)))
        else:
            new_h = image_scale
            r = new_h / h
            new_w = w * r
            image = cv2.resize(image, (round(new_w), round(new_h)))
    return image, r
