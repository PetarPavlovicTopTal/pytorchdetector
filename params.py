import torch

from lib.model.FasterRCNN import get_mobilenet_v2_faster, get_resnet50_faster, get_shufflenet_v2_faster, get_custom_mobilenet_v2_faster
from lib.time import get_time
import lib.transforms as T


class Params:

    @staticmethod
    def get_transform(train):
        transforms = []
        transforms.append(T.ToTensor())
        if train:
            transforms.append(T.RandomHorizontalFlip(0.5))
        return T.Compose(transforms)

    data_root = '/media/david/A/Datasets/PvP/dataset/training_on_this/7.4.'
    images_at = 'images'
    annotations_at = 'annotations'

    num_classes = 2

    class Train:
        batch_size = 2
        shuffle    = True
        num_workers = 4
    class Test:
        batch_size  = 1
        shuffle     = False
        num_workers = 4

    # model = get_mobilenet_v2_faster(num_classes)
    model = torch.load('models/2021-3-24_0-22-52/60_full_model.pt')

    class Optimizer:
        func         = torch.optim.SGD
        lr           = 0.0003
        momentum     = 0.9
        weight_decay = 0.0005

    T_0 = 1
    T_mult = 2

    num_epochs = 10000

    print_freq = 500
    eval_every_n_epoch = 1

    save_dir = 'models/' + get_time()
    save_every_n_epochs = 1
