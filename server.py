import argparse
import socket
import cv2
import os
import torch
from torchvision.transforms import functional as F
from IPython import embed # TODO remove
import numpy as np
from lib.data.classes import Classes
from lib.centroid_tracker import CentroidTracker


def show(image): # TODO remove
    cv2.imshow('', image)
    cv2.waitKey()
    cv2.destroyAllWindows()


def process_frame(model, frame, score_thr):
    image = frame
    image_tensor = F.to_tensor(image)
    image_tensor = image_tensor.unsqueeze(0)
    image_tensor = image_tensor.to(device)

    predictions = model(image_tensor)

    predictions = predictions[0]

    bboxes = predictions['boxes'].cpu().detach().numpy()
    labels = predictions['labels'].cpu().numpy()
    scores = predictions['scores'].cpu().detach().numpy()

    bboxes = bboxes[scores > score_thr]
    labels = labels[scores > score_thr]
    scores = scores[scores > score_thr]

    class_names = np.array([Classes.reverse_joined_classes[x] for x in labels])

    return class_names, bboxes


def service_client(
        client_socket,
        model,
        score_threshold,
        buffer_size,
        max_detections,
        separator,
):
    while True:
        received = client_socket.recv(50)

        print (f'Received {received}')

        if len(received) == 0:
            print ('Breaking')
            return

        received = received.decode().split('|', 1)[0]

        frame_n, file_size, frame_shape = received.split(separator)

        frame_shape = [int(x) for x in frame_shape.split('(', 1)[1].rsplit(')', 1)[0].replace(' ', '').split(',')]

        file_size = int(file_size)

        print (frame_n, file_size)

        frame = np.array([]).reshape(-1)
        received_size = 0
        while True:
            bytes_read = client_socket.recv(buffer_size)
            received_size += len(bytes_read)
            if not bytes_read:
                break

            frame = np.append(frame, np.frombuffer(bytes_read, dtype=np.uint8))

            if received_size >= file_size:
                break

        client_socket.send('SUCCESS'.encode())

        frame = frame.astype(np.uint8)
        frame_reshaped = frame.reshape(frame_shape)

        class_names, detections = process_frame(model, frame_reshaped, score_threshold)
        detections = centroid_tracker.match_last_frame_detections(detections)

        print(detections)

        if detections.shape[0] == 0:
            detections = np.ones((max_detections, 5), dtype=np.float32) * -1
        elif detections.shape[0] >= max_detections:
            detections = detections[:max_detections]
        else:
            fill_detections = np.ones((max_detections - detections.shape[0], 5), dtype=np.float32) * -1
            detections = np.vstack((detections, fill_detections))
        detections = detections.astype(np.float32)

        byte_detections = detections.tobytes()
        client_socket.send(byte_detections)

        # show(frame_reshaped)



def get_args():

    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--model',
        type=str,
        default='models/7_full_model.pt', # TODO change to 7_full_model.pt
        dest='model',
        help='Path to model that will be used.',
    )
    parser.add_argument(
        '--score_threshold',
        type=float,
        default=0.7,
        dest='score_threshold',
        help='All detections with score above the threshold will be accepted.',
    )
    parser.add_argument(
        '--host_ip',
        type=str,
        default='0.0.0.0', # TODO remove
        dest='host_ip',
        help='Server IP that server will host on.',
    )
    parser.add_argument(
        '-p', '--port',
        type=int,
        default=5001,
        dest='port',
        help='Port that the server will listen to.',
    )
    parser.add_argument(
        '--buffer_size',
        type=int,
        default=4096,
        dest='buffer_size',
        help='Amount of bytes that will be received from client with each package. Must be the same on the client side.',
    )
    parser.add_argument(
        '--separator',
        type=str,
        default='<>',
        dest='separator',
        help='Separator used for exchanging messages. Must be the same on the client side.',
    )
    parser.add_argument(
        '--supported_clients_number',
        type=int,
        default=1,
        dest='supported_clients_number',
        help='CHANGE WITH CAUTION! Number of clients that can connect at the same time.',
    )
    parser.add_argument(
        '--max_detections',
        type=int,
        default=100,
        dest='max_detections',
        help='CHANGE WITH CAUTION! Number needs to be synced with detections_buffer_size param on clients side.'
             'Maximum detections that can be send to the client. If more detections are found, only max_detections will'
             'be returned. Max detections will always be send. If there are less then max_detections, dummy detection'
             'will be send.'
    )
    return parser.parse_args()


if __name__ == '__main__':

    args = get_args()

    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

    model = torch.load(args.model)
    model.to(device)
    model.eval()

    s = socket.socket()

    s.bind((args.host_ip, args.port))

    s.listen(args.supported_clients_number)

    print(f'[*] Listening as {args.host_ip}:{args.port}')

    try:
        try:
            while True:
                client_socket, address = s.accept()

                print(f'[+] {address} is connected.')

                centroid_tracker = CentroidTracker()

                service_client(
                    client_socket,
                    model,
                    args.score_threshold,
                    args.buffer_size,
                    args.max_detections,
                    args.separator,
                )
                client_socket.close()
        except:
            client_socket.close()
    except:
        print ('Closing the server.')
        s.close()
