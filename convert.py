import os
import torch
from params import Params as p

load_path = 'models/2021-3-2_20-40-8/end.pt'
save_path = 'models/2021-3-2_20-40-8/end_full_model.pt'

# Load
model = p.model
model.load_state_dict(torch.load(load_path))
model.eval()

torch.save(model, save_path)
