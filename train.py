import os
import torch

from lib.data.MarkDataset import MarkDataset
from lib.engine import train_one_epoch, evaluate
import lib.utils as utils
from params import Params as p


device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')


dataset = MarkDataset(
    p.data_root,
    p.images_at,
    p.annotations_at,
    p.get_transform(train=True),
)
dataset_test = MarkDataset(
    p.data_root,
    p.images_at,
    p.annotations_at,
    p.get_transform(train=False),
)

indices = torch.randperm(len(dataset)).tolist()

dataset = torch.utils.data.Subset(dataset, indices[:-1000])
dataset_test = torch.utils.data.Subset(dataset_test, indices[-1000:])

data_loader_train = torch.utils.data.DataLoader(
                                    dataset,
                                    batch_size  = p.Train.batch_size,
                                    shuffle     = p.Train.shuffle,
                                    num_workers = p.Train.num_workers,
                                    collate_fn  = utils.collate_fn,
                                )

data_loader_test = torch.utils.data.DataLoader(
                                    dataset_test,
                                    batch_size  = p.Test.batch_size,
                                    shuffle     = p.Test.shuffle,
                                    num_workers = p.Test.num_workers,
                                    collate_fn  = utils.collate_fn,
                                )

model = p.model
model.to(device)

params = [p for p in model.parameters() if p.requires_grad]

optimizer = p.Optimizer.func(
    params,
    lr           = p.Optimizer.lr,
    momentum     = p.Optimizer.momentum,
    weight_decay = p.Optimizer.weight_decay,
)

lr_scheduler = torch.optim.lr_scheduler.CosineAnnealingWarmRestarts(optimizer, T_0=p.T_0, T_mult=p.T_mult)

num_epochs = p.num_epochs

for epoch in range(num_epochs):
    train_one_epoch(model, optimizer, data_loader_train, device, epoch, print_freq=p.print_freq)

    lr_scheduler.step()

    if epoch % p.eval_every_n_epoch == 0:
        evaluate(model, data_loader_test, device=device)

    if epoch % p.save_every_n_epochs == 0:
        if not os.path.exists(p.save_dir):
            os.makedirs(p.save_dir)
        save_path = os.path.join(p.save_dir, str(epoch)) + '.pt'
        torch.save(model.state_dict(), save_path)
        save_path = os.path.join(p.save_dir, str(epoch)) + '_full_model.pt'
        torch.save(model, save_path)
        print("Model saved at {}".format(save_path))

    print('')
    print('==================================================')
    print('')

evaluate(model, data_loader_test, device=device)

save_path = os.path.join(p.save_dir, 'end.pt')
torch.save(model.state_dict(), save_path)
print("Model saved at {}".format(save_path))
print("That's it!")
