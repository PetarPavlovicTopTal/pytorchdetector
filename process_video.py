import argparse
import cv2
import os
import torch
from PIL import Image
from torchvision.transforms import functional as F
from IPython import embed
import time
import numpy as np
from lib.visualize import visualize_detections
from lib.data.classes import Classes
from lib.image import resize_np
from lib.nms import NMS
from lib.centroid_tracker import CentroidTracker


def process_frame(model, frame, score_thr):
    image = frame
    image_tensor = F.to_tensor(image)
    image_tensor = image_tensor.unsqueeze(0)
    image_tensor = image_tensor.to(device)

    predictions = model(image_tensor)

    predictions = predictions[0]

    bboxes = predictions['boxes'].cpu().detach().numpy()
    labels = predictions['labels'].cpu().numpy()
    scores = predictions['scores'].cpu().detach().numpy()

    bboxes = bboxes[scores > score_thr]
    labels = labels[scores > score_thr]
    scores = scores[scores > score_thr]

    class_names = np.array([Classes.reverse_joined_classes[x] for x in labels])

    # if len(bboxes) > 3:
    #     embed()
    #     exit()
    # print (bboxes.shape)
    #
    # scores_arg = scores.argsort()
    # scores = scores[scores_arg]
    # bboxes = bboxes[scores_arg]
    # bboxes = np.array(NMS(bboxes))

    return class_names, bboxes


def write_text(image, detections):

    font = cv2.FONT_HERSHEY_SIMPLEX
    fontScale = 1
    fontColor = (255, 255, 255)
    thickness = 2
    lineType = 2

    colors = {
        1: (0, 255, 0),
        2: (0, 0, 255),
        3: (255, 0, 0),
        4: (255, 255, 0),
        5: (255, 0, 255),
    }

    for detection in detections:
        location = (int(detection[0]), int(detection[1]) - 5)
        if detection[4] < 10:
            image[location[1] - 28:location[1] + 5, location[0] - 3: location[0] + 25] = colors[
                (detection[4]% len(colors)) + 1]
        else:
            image[location[1] - 28:location[1] + 5, location[0] - 3: location[0] + 45] = colors[
                (detection[4] % len(colors)) + 1]
        cv2.putText(
            image,
            str(int(detection[4])),
            location,
            font,
            fontScale,
            fontColor,
            thickness,
            lineType)


def draw_boxes(image, bboxes):
    colors = {
        1: (0, 255, 0),
        2: (0, 0, 255),
        3: (255, 0, 0),
        4: (255, 255, 0),
        5: (255, 0, 255),
    }

    for i, box in enumerate(bboxes):
        image = cv2.rectangle(image, (int(box[0]), int(box[1])), (int(box[2]), int(box[3])), colors[(box[4]% len(colors)) + 1], 2)


def process_video(
        model           : str,
        video_path      : str,
        output_path     : str,
        rotate          : int,
        save_every_n    : int,
        score_threshold : float,
        fps             : int,
) -> None:
    assert rotate in [0, 90, 180, 270]
    assert save_every_n > 0

    rotate_flag = 0
    if rotate == 90:
        rotate_flag = cv2.ROTATE_90_CLOCKWISE
    elif rotate == 180:
        rotate_flag = cv2.ROTATE_180
    elif rotate == 270:
        rotate_flag = cv2.ROTATE_90_COUNTERCLOCKWISE

    cap = cv2.VideoCapture(video_path)
    fourcc = cv2.VideoWriter_fourcc(*'MP4V')

    centroid_tracker = CentroidTracker()

    i = 0
    n = 0
    while (cap.isOpened()):
        ret, frame = cap.read()

        if i == 0:
            out = cv2.VideoWriter(output_path + '.mp4', fourcc, fps, (frame.shape[1], frame.shape[0]))

        if ret == False:
            break
        i += 1
        if i % save_every_n == 0:
            n += 1
            print('Extracting {}... {}'.format(video_path[-50:], n), end='\r')
            if i < 2450: continue
            if i > 2784: break
            if rotate:
                frame = cv2.rotate(frame, rotate_flag)

            resized_frame, r = resize_np(frame, 720)

            class_names, detections = process_frame(model, resized_frame, score_threshold)

            detections = detections / r

            detections = centroid_tracker.match_last_frame_detections(detections)

            draw_boxes(frame, detections)
            write_text(frame, detections)

            out.write(frame)

    cap.release()
    out.release()
    cv2.destroyAllWindows()


def get_args():

    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--model',
        type=str,
        dest='model',
    )
    parser.add_argument(
        '--video',
        type=str,
        help='Path to directory with video files.',
    )
    parser.add_argument(
        '--output',
        type=str,
        help='Path to file where output video will be saved.',
    )
    parser.add_argument(
        '--rotate',
        type=int,
        default=0,
        help='If images need to be rotated set the clockwise angle to be rotated. '
             'Possible angles are [0, 90, 180, 270].',
    )
    parser.add_argument(
        '--fps',
        type=int,
        default=30,
        help='Output video fps.',
    )
    parser.add_argument(
        '--every_n',
        type=int,
        default=1,
        help='Save every n image. Must be a positive number.',
    )
    parser.add_argument(
        '--score_threshold',
        type=float,
        default=0.7,
    )
    return parser.parse_args()


if __name__ == '__main__':

    args = get_args()

    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

    model = torch.load(args.model)
    model.to(device)
    model.eval()

    process_video(
        model           = model,
        video_path      = args.video,
        output_path     = args.output,
        rotate          = args.rotate,
        save_every_n    = args.every_n,
        score_threshold = args.score_threshold,
        fps             = args.fps,
    )
