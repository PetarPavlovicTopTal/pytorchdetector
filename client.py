import argparse
import cv2
import numpy as np
import socket


def resize_np(image, image_scale):
    h, w, _ = image.shape
    r = 1
    if w != image_scale or h != image_scale:
        if w > h:
            new_w = image_scale
            r = new_w / w
            new_h = h * r
            image = cv2.resize(image, (round(new_w), round(new_h)))
        else:
            new_h = image_scale
            r = new_h / h
            new_w = w * r
            image = cv2.resize(image, (round(new_w), round(new_h)))
    return image, r


def write_text(image, detections):

    font = cv2.FONT_HERSHEY_SIMPLEX
    fontScale = 1
    fontColor = (255, 255, 255)
    thickness = 2
    lineType = 2

    colors = {
        1: (0, 255, 0),
        2: (0, 0, 255),
        3: (255, 0, 0),
        4: (255, 255, 0),
        5: (255, 0, 255),
    }

    for detection in detections:
        location = (int(detection[0]), int(detection[1]) - 5)
        if detection[4] < 10:
            image[location[1] - 28:location[1] + 5, location[0] - 3: location[0] + 25] = colors[
                (detection[4]% len(colors)) + 1]
        else:
            image[location[1] - 28:location[1] + 5, location[0] - 3: location[0] + 45] = colors[
                (detection[4] % len(colors)) + 1]
        cv2.putText(
            image,
            str(int(detection[4])),
            location,
            font,
            fontScale,
            fontColor,
            thickness,
            lineType)


def draw_boxes(image, bboxes):
    colors = {
        1: (0, 255, 0),
        2: (0, 0, 255),
        3: (255, 0, 0),
        4: (255, 255, 0),
        5: (255, 0, 255),
    }

    for i, box in enumerate(bboxes):
        image = cv2.rectangle(image, (int(box[0]), int(box[1])), (int(box[2]), int(box[3])), colors[(box[4]% len(colors)) + 1], 2)


def show(image):
    cv2.imshow('', image)
    cv2.waitKey()
    cv2.destroyAllWindows()


def process_stream(
        _socket,
        stream,
        port,
        buffer_size,
        detections_buffer_size,
        separator,
        save,
        show_im,
        output_size,
        fps,
        scale_input,
):
    try:
        cap = cv2.VideoCapture(stream)
        frame_n = 0

        if save:
            fourcc = cv2.VideoWriter_fourcc(*'MP4V')

        while (cap.isOpened()):
            ret, frame = cap.read()

            if not ret:
                print ('end')
                break

            if frame_n == 0 and save:
                out = cv2.VideoWriter(save + '.mp4', fourcc, fps, (frame.shape[1], frame.shape[0]))

            frame_resized, r = resize_np(frame, scale_input)

            frame_n += 1

            print (f'frame_n = {frame_n}')

            byte_frame = frame_resized.tobytes()

            byte_frame_len = len(byte_frame)

            header_message = f'{frame_n}{separator}{byte_frame_len}{separator}{frame_resized.shape}'
            header_message += '|' * (50 - len(header_message))

            print(f"Sending {header_message}")

            _socket.send(header_message.encode())

            chunks = byte_frame_len // buffer_size
            x = 0
            while True:
                _socket.sendall(byte_frame[x * buffer_size : (x + 1) * buffer_size])
                x += 1
                print (f'Sending image {x} / {chunks}', end='\r')

                if x * buffer_size > byte_frame_len:
                    break

            message = _socket.recv(7).decode()
            if message != 'SUCCESS':
                print(f'{i} | {message}')

            detections = np.frombuffer(_socket.recv(detections_buffer_size), dtype=np.float32)

            detections = detections.reshape(-1, 5)

            detections_index = np.where(detections[:, -1] != -1)[0]

            detections = detections[detections_index]

            detections[:, :4] = detections[:, :4] / r

            draw_boxes(frame, detections)
            write_text(frame, detections)

            if output_size:
                frame, _ = resize_np(frame, output_size)

            if show_im:
                show(frame)

            if save:
                out.write(frame)

        _socket.close()
        if save:
            out.release()

    except Exception as e:
        print (f'Error {e}')
        _socket.close()
        if save:
            out.release()


def get_args():

    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--stream',
        type=str,
        default='/media/david/A/Datasets/PvP/Test videos/gun4_2.mp4', # TODO remove
        dest='stream',
        help='Stream link or path to video file.',
    )
    parser.add_argument(
        '--host',
        type=str,
        default='127.0.0.1', # TODO remove
        dest='host',
        help='Server IP address.',
    )
    parser.add_argument(
        '-p', '--port',
        type=int,
        default=5001, # TODO remove
        dest='port',
        help='Define port on which server will listen. Must be the same on server side.',
    )
    parser.add_argument(
        '--buffer_size',
        type=int,
        default=4096,
        dest='buffer_size',
        help='Amount of bytes that will be send to server with each package. Must be the same on server side.',
    )
    parser.add_argument(
        '--detections_buffer_size',
        type=int,
        default=2000,
        dest='detections_buffer_size',
        help='Amount of bytes that server sends back with detections. Must be the same on server side.',
    )
    parser.add_argument(
        '--separator',
        type=str,
        default='<>',
        dest='separator',
        help='Separator used for exchanging messages. Must be the same on server side.',
    )
    parser.add_argument(
        '--scale_input',
        type=int,
        default=720,
        dest='scale_input',
        help='CHANGE WITH CAUTION! This param changes input image size. '
             'If effects neural network input image size. Default: 720.'
    )
    parser.add_argument(
        '--output_size',
        type=int,
        default=None,
        dest='output_size',
        help='Resize processed frame for ease of use.',
    )
    parser.add_argument(
        '--fps',
        type=int,
        default=30,
        help='Output video fps.',
    )
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument(
        '--save',
        type=str,
        dest='save',
        help='Path to mp4 file where output will be saved. Path without extension! Mutually exclusive with \'show\' option.',
    )
    group.add_argument(
        '--show',
        action='store_true',
        dest='show',
        help='Show detections after each frame. Mutually exclusive with \'save\' option.',
    )
    return parser.parse_args()


if __name__ == '__main__':

    args = get_args()

    _socket = socket.socket()

    print(f'Connecting to {args.host}:{args.port}')

    _socket.connect((args.host, args.port))

    print("Connected.")

    process_stream(
        _socket,
        args.stream,
        args.port,
        args.buffer_size,
        args.detections_buffer_size,
        args.separator,
        args.save,
        args.show,
        args.output_size,
        args.fps,
        args.scale_input,
    )
